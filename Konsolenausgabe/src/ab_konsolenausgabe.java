
public class ab_konsolenausgabe {

	public static void main(String[] args) {
		
		
		// aufgabe 1
		String s = "**";
		String ss = "*";
		
		System.out.println("-------------");
		System.out.println("| Aufgabe 1 |");
		System.out.println("-------------");
		System.out.println(" ");
		
		System.out.printf( "%20s\n", s );  
		System.out.printf( "%15s %8s \n", ss, ss );
		System.out.printf( "%15s %8s \n", ss, ss );
		System.out.printf( "%20s\n", s );
		System.out.println(" ");
		
			
		// aufgabe 2
		
		System.out.println("-------------");
		System.out.println("| Aufgabe 2 |");
		System.out.println("-------------");
		System.out.println(" ");
		
		int n0 = 0;
		int n1 = 1;
		int n2 = 2;
		int n3 = 3;
		int n4 = 4;
		int n5 = 5;
		String F = "%4d\n";
		
		System.out.printf("0!   " + "=" + "                   " + "=" + F,n0*n1);
		System.out.printf("1!   " + "=" + " 1                 " + "=" + F,n1*n1);
		System.out.printf("2!   " + "=" + " 1 * 2             " + "=" + F,n1*n2);
		System.out.printf("3!   " + "=" + " 1 * 2 * 3         " + "=" + F,n1*n2*n3);
		System.out.printf("4!   " + "=" + " 1 * 2 * 3 * 4     " + "=" + F,n1*n2*n3*n4);
		System.out.printf("5!   " + "=" + " 1 * 2 * 3 * 4 * 5 " + "=" + F,n1*n2*n3*n4*n5);
		System.out.println(" ");
				
		
		// anfang aufgabe 3
		System.out.println("-------------");
		System.out.println("| Aufgabe 3 |");
		System.out.println("-------------");
		System.out.println(" ");
		
		// text- und formatierungszeichen in String variablen angelegt
		String a = "Fahrenheit";
		String b = "Celsius";
		String x = "%-12s | %10s \n";
		String y = "%-12d | %10.2f \n";
		String z = "%+-12d | %10.2f \n";
		
		// alle zahlen in integer und double variablen angelegt
		int c = -20;
		double d = -28.8889;
		int e = -10;
		double f = -23.3333;
		int g = 0;
		double h = -17.7778;
		int i = 20;
		double j = -6.6667;
		int k = 30;
		double l = -1.1111;
		
		// das eigentliche Programm
		System.out.printf(x, a, b);
		System.out.println("--------------------------");
		System.out.printf(y, c, d);
		System.out.printf(y, e, f);
		System.out.printf(z, g, h);
		System.out.printf(z, i, j);
		System.out.printf(z, k, l);
		
		
		

	}

}

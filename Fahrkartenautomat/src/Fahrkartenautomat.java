﻿import java.util.Scanner;

class Fahrkartenautomat {
	// methoden
	// ==========================================================================

	static double fahrkartenbestellungErfassen(int anzahlFahrkarten, double zuZahlenderBetrag) {

		double gesamtbetrag = anzahlFahrkarten * zuZahlenderBetrag;

		return gesamtbetrag;
	}

	static double fahrkartenBezahlen(double gesamt, double eingezahlt) {
		double eingezahltSumme;
		eingezahltSumme = gesamt - eingezahlt;
		return eingezahltSumme;
	}

	static void warte(int millisekunde) {

		for (int i = 0; i < 40; i++) {
			System.out.print("=");
			try {
				Thread.sleep(millisekunde);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	static double rueckgeldAusgeben(double eingezahlterGesamtbetrag, double gesamtbetrag) {

		double rückgabebetrag = eingezahlterGesamtbetrag - gesamtbetrag;

		return rückgabebetrag;
	}

	static void muenzeAusgeben(double betrag, String einheit) {

		if (betrag > 0.0) {
			System.out.printf("Der Rückgabebetrag in Höhe von " + "%.2f" + " Euro \n", betrag);
			System.out.println("wird in folgenden Münzen ausgezahlt:");

			while (betrag >= 1.99) // 2 EURO-Münzen
			{
				System.out.printf("2" + "%s\n", einheit);
				betrag -= 2.0;
			}
			while (betrag >= 0.99) // 1 EURO-Münzen
			{
				System.out.printf("1" + "%s\n", einheit);
				betrag -= 1.0;
			}
			while (betrag >= 0.49) // 50 CENT-Münzen
			{
				System.out.printf("0,50" + "%s\n", einheit);
				betrag -= 0.5;
			}
			while (betrag >= 0.19) // 20 CENT-Münzen
			{
				System.out.printf("0,20" + "%s\n", einheit);
				betrag -= 0.2;
			}
			while (betrag >= 0.09) // 10 CENT-Münzen
			{
				System.out.printf("0,10" + "%s\n", einheit);
				betrag -= 0.1;
			}
			while (betrag >= 0.04)// 5 CENT-Münzen
			{
				System.out.printf("0,05" + "%s\n", einheit);
				betrag -= 0.05;
			}
		}
	}

	// ==========================================================================

	// hauptprogramm
	// ==========================================================================
	public static void main(String[] args) {
		Scanner tastatur = new Scanner(System.in);

		// Geldeinwurf und auswahl der fahrkartenmenge
		// ======================================================
		// mit methode
		// ========================================================
		// fahrkarten bestellung
		// =======================================================

		boolean neuStart = true;

		while (neuStart == true) {

			int fahrkartenmenge;
			int auswahl;
			double preisFahrkarte = 0;
			boolean richtigeFahrkartenwahl = false;
			double gesamtbetrag = 0;
			double einzelfahrschein = 2.90;
			double tageskarte = 8.60;
			double kleingruppenTageskarte = 23.50;

			System.out.println(">>> PROGRAMMSTART <<<\n");
			System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n"
					+ "Einzelfahrschein Regeltarif AB [2,90 EUR] (1)\n" + "Tageskarte Regeltarif AB [8,60 EUR] (2)\n"
					+ "Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\n");

			while (richtigeFahrkartenwahl == false) {

				System.out.print("Ihre Wahl: ");
				auswahl = tastatur.nextInt();

				if (auswahl == 1 || auswahl == 2 || auswahl == 3) {
					richtigeFahrkartenwahl = true;
					if (auswahl == 1) {
						preisFahrkarte = einzelfahrschein;
					}
					if (auswahl == 2) {
						preisFahrkarte = tageskarte;
					}
					if (auswahl == 3) {
						preisFahrkarte = kleingruppenTageskarte;
					}
				} else {
					System.out.println(">>falsche Eingabe<<");
				}
			}

			System.out.print("Anzahl der Tickets: ");
			fahrkartenmenge = tastatur.nextInt();
			gesamtbetrag = (fahrkartenbestellungErfassen(fahrkartenmenge, preisFahrkarte));

			// fahrkarten bezahlen
			// ==========================================================

			double eingezahlterGesamtbetrag = 0.0;
			double eingeworfeneMünze;

			while (eingezahlterGesamtbetrag < gesamtbetrag) {
				System.out.printf("Noch zu zahlen: " + "%.2f" + " Euro \n", (fahrkartenBezahlen(gesamtbetrag, eingezahlterGesamtbetrag)));
				System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
				eingeworfeneMünze = tastatur.nextDouble();
				eingezahlterGesamtbetrag += eingeworfeneMünze;
			}

			// Fahrscheinausgabe
			// -----------------
			System.out.println("\nFahrschein wird ausgegeben");

			warte(4); // 1000 = 1sec

			System.out.println("\n\n");

			// Rückgeldberechnung und -Ausgabe
			// -------------------------------

			double geldZurueck;
			geldZurueck = rueckgeldAusgeben(eingezahlterGesamtbetrag, gesamtbetrag);

			muenzeAusgeben(geldZurueck, " Euro");

			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir wünschen Ihnen eine gute Fahrt.\n\n" + ">>> PROGRAMMENDE <<<\n");

		}
	}
}